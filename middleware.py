from mockchain3 import gen_chain


# The base "Blockchain" class is given a list of blocks, and it is
# supposed to be able to provide the account balances as well as
# calculating the median of the account balances

class Blockchain(object):
    def __init__(self, block_list):
        self._blocks = {}
        self.head = None

        for block in block_list:
            self._blocks[block.hash] = block
            self._process_block(block)


# This implementation simply takes advantage of the fact that
# every `Block` object contains already an instance of `Accounts`
# with the state at that point. With that in mind, there is no
# real need to recalculate anything. All we need to do is to keep
# track of what block is supposed to be the head.            
class Cheatingchain(Blockchain):
    def _can_become_head(self, block):
        return any([
            self.head is None,
            self.head and block.prevhash == self.head.hash,
            self.head and self.head.number < block.number
        ])

    def _process_block(self, block):
        if self._can_become_head(block):
            self.head = block

    @property
    def median(self):
        return self.head and self.head.accounts.median()

    @property
    def balances(self):
        return self.head and self.head.accounts.balances


# This implementation actually exercises the point of finding
# needed regorganizations and re-calculating balances.
class Properchain(Blockchain):
    def __init__(self, block_list):
        # The initial balances have been generated with the genesis
        # block. Let's just copy those here.
        self._balances = block_list[0].accounts.balances.copy()

        super(Properchain, self).__init__(block_list)

    def _can_become_head(self, block):
        return self.head is None or all([
            block.prevhash in self._blocks,
            self.head and block.prevhash == self.head.hash,
            self.head and self.head.number < block.number
        ])

    def _is_reorg_needed(self, block):
        return self.head is not None and all([
            block.prevhash != self.head.hash,
            block.number > self.head.number
        ])

    def _reorganize(self, block):
        # We will go up on both the forked chain and the existing
        # chain, reverting all of the transactions of the current chain

        current = self.head
        forked = block

        # To keep track of the blocks that will be part of the new
        # chain and will need to have the transfers applied.

        new_chain = []

        while forked.prevhash != current.prevhash and current.number > 0:
            self._revert_transfers(current)

            forked = self._blocks[forked.prevhash]
            current = self._blocks[current.prevhash]

            new_chain.append(forked)

        # Here we should be at the point of the fork, new chain will
        # have a list of the new elements, child-to-parent

        for b in new_chain[::-1]:  # reversed order.
            self.head = b
            self._apply_transfers(b)

    def _process_block(self, block):
        if self._is_reorg_needed(block):
            self._reorganize(block)

        if self._can_become_head(block):
            self.head = block
            self._apply_transfers(block)

    def _apply_transfers(self, block):
        for transfer in block.transfers:
            self._balances[transfer.sender] -= transfer.amount
            self._balances[transfer.receiver] += transfer.amount

    def _revert_transfers(self, block):
        for transfer in block.transfers:
            self._balances[transfer.sender] += transfer.amount
            self._balances[transfer.receiver] -= transfer.amount

    @property
    def balances(self):
        return self._balances

    @property
    def median(self):
        return sorted(self.balances.values())[int(len(self.balances) / 2)]


if __name__ == '__main__':
    chain = gen_chain()

    cc = Cheatingchain(chain)
    pc = Properchain(chain)

    print('Cheating median', cc.median)
    print('Proper median', pc.median)

    print('Cheating supply', sum(cc.balances.values()))
    print('Proper supply', sum(pc.balances.values()))

    print('Matching balances: {}'.format(pc.balances == cc.balances))
