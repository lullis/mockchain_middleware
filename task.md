**Draft and implement efficient data structures for the middleware, to
store account balances and calculate the median.**

These were already provided on the code provided. I am also
submitting a modified mockchain3.py file, which contains some
modifications to get the code to work in python3


**Write code to read and replay test chain data to update these data
structures and export the balances as they are after the last block**

Please refer to middleware.py. There are two implementations there.

The first one (CheatingChain) simply uses the structures that are
defined for the generation of the chain. I am exploiting the fact that
every block being created already contains a dictionary with all of
the balances + transfers. So this implementation doesn't really do
anything, except keeping track of what should be the head node.

The second one (ProperChain) does the work of reading the transfers of
the blocks and actually keeps one single dictionary with all account
balances. When an incoming block is found to be trigger a chain
reorganization, this implementation reverts all of the transactions
until it finds the common ancestors between new block and current
head, then "goes down" the new chain applying the transfers of the new
chain.

By running `python3 middleware.py`, you should get the execution of
both implementations on the same mocked chain, and the results should
match.


**Describe an efficient solution for above with the constraint, that
the complete data structure doesn’t fit into**

Instead of simply storing things in a dictionary, one use some sort of
KV store (BerkeleyDB, LevelDB, RocksDB, etc). Also possible (with
slightly more complex implementation) would be to use a Relational
database and create tables to record transfers, blocks and accounts.

**Describe what needs to be considered in the client app.**

Answer: The client needs to be able to query the middleware for the
state of the balances and of any possible chain regorganization. It
also should be able to store identifiers of all of its own
transactions, in order to be able to query the middleware about it.

**Describe which edge cases you can think about.**

 - Network partitions between middleware and ethereum node, or
   ethereum node and rest of network.
 - Middleware going offline and client not being able to get any kind
   of updates.

**Describe a system architecture and communication pattern for updating
the client app**

One possible solution would be to use a publish-subscribe pattern,
where the middleware publishes messages of all new blocks, as well as
of transactions that contains on this block. In this way, clients
could subscribe to messages of new blocks, as well as to get
information about the transactions and to know how many confirmations
they have.

**Which fields of computer science address the described problem? What
are the standard patterns for this (if any)?**

For distributed systems, most of the standard messaging patterns rely
on either the request-response or on the one-way pattern.
